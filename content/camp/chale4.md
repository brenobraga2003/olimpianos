+++
title = 'Chalé 4: Deméter'
date = 2023-09-28T16:01:28-03:00
draft = false
+++
**O chalé é feito de um mármore escuro e amarronzado, possui tomateiros nas paredes e uma cobertura feita de grama de verdade. Árvores das mais variadas frutas circundam o chalé, seu galhos entrando pelas janelas quase sempre abertas. Tem uma grande árvore (carvalho), no meio do chalé, sustentando o teto.**