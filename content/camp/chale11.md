+++
title = 'Chalé 11: Hermes'
date = 2023-09-28T15:59:50-03:00
draft = false
+++
**Um chalé de acampamento normal, velho, com a pintura descascando e um caduceu - muito confundido com o símbolo da medicina, porém este tem apenas uma cobra e o de Hermes, duas - acima da porta. É o mais lotado, pois além dos filhos de Hermes, lá também dormem meio-sangues indeterminados.**