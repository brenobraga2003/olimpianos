+++
title = 'Chalé 6: Atena'
date = 2023-09-28T16:01:02-03:00
draft = false
+++
**O chalé tem um mármore pálido e branco, ornado com grossas colunas torcidas em tranças e vários galhos da enorme oliveira que fica á oeste do lugar. Tem um brilho fraco durante qualquer período do dia, e suas paredes interiores são atravancadas de livros. Tem uma coruja entalhada na frente, com olhos de ônix.**