+++
title = 'Chalé 9: Hefesto'
date = 2023-09-28T16:00:22-03:00
draft = false
+++
**Um chalé negro, com chaminés no telhado. É frequente o barulho de ferro contra pedra lá dentro. Parece ter apenas um andar por fora, mas por dentro foi totalmente equipado tendo-se conhecimento de dois andares e vários tuneis, e uma enorme forja.**