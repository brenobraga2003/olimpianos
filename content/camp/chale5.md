+++
title = 'Chalé 5: Ares'
date = 2023-09-28T16:01:15-03:00
draft = false
+++
**O chalé é de um vermelho-vivo mal pintado, tem seu telhado forrado de arame farpado, com uma cabeça de javali empalhada acima da porta central. Espadas e facões foram entalhados no mármore escuro e decoram as paredes do lugar. Quase sempre pode se ouvir rock de dentro do chalé.**