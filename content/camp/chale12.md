+++
title = 'Chalé 12: Dionísio'
date = 2023-09-28T15:58:26-03:00
draft = false
+++
**O chalé cor de vinho, escuro, com desenhos de parreiras por toda a parte, tem um mini freezer com diet coke, e variados tipos de comida, uma estátua de Dionísio em mármore e olhos de ametista no meio do chalé, (e um lugar para guardar "apetrechos de festas").**