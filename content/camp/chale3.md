+++
title = 'Chalé 3: Poseidon'
date = 2023-09-28T16:01:40-03:00
draft = false
+++
**O chalé é comprido, baixo e sólido. As paredes externas são de pedras cinzentas rústicas, salpicadas de pedaços de conchas e coral. As paredes internas são feitas de madrepérola.Tem uma fonte d'água com dracmas de ouro dentro.**