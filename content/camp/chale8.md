+++
title = 'Chalé 8: Ártemis'
date = 2023-09-28T16:00:37-03:00
draft = false
+++
**Um chalé de acampamento aparentemente normal durante o dia, mas que possui um fulgor prateado no escuro. Imagens de animais e lanças enfeitam as paredes. O chalé é usado quando as Caçadoras precisam de um lugar seguro para ficar enquanto Ártemis faz caçadas perigosas.**