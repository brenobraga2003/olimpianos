+++
title = 'Chalé 10: Afrodite'
date = 2023-09-28T16:00:10-03:00
draft = false
+++
**Um chalé rosa pastel, com cristais pendendo das janelas. Há vários canteiros de rosas - sem espinhos - das mais variadas cores por perto, e estas aninham-se nas paredes. Um perfume suave e adocicado está por toda a parte, tem vários espelhos.**