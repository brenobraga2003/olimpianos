+++
title = 'Chalé 7: Apolo'
date = 2023-09-28T16:00:51-03:00
draft = false
+++
**O chalé parece que é de ouro sólido, há um arco dourado na parte da frente e o lugar brilha fortemente durante o dia ,que chega a doer os olhos. As paredes foram talhadas com milhares de notas musicais, sobressaindo-se a clave de sol. Tem flores da ilha de Delos, que só cresce nesse chalé.**