+++
title = 'A Maldição do Titã'
date = 2023-09-28T16:01:58-03:00
draft = false
+++
![](/images/Livro3.jpeg)

**"A oeste, cinco buscarão a deusa acorrentada**

**Um se perderá na terra ressecada**

**A desgraça do Olimpo aponta a trilha**

**Campistas e Caçadoras, cada um brilha**

**A maldição do titã um deve sustentar**

**E pela mão do pai, um irá expirar."**