+++
title = 'O Último Olimpiano'
date = 2023-09-28T16:01:56-03:00
draft = false
+++
![](/images/Livro5.jpeg)

**"Um meio-sangue, dos deuses antigos filho,**

**Chegará aos dezesseis apesar de empecilhos**

**Num sono sem fim o mundo estará**

**E a alma do herói, a lâmina maldita ceifará**

**Uma escolha seus dias vai encerrar**

**O Olimpo preservar ou arrasar."**