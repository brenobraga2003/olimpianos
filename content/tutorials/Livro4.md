+++
title = 'A Batalha no Labirinto'
date = 2023-09-28T16:01:57-03:00
draft = false
+++
![](/images/Livro4.jpeg)

**"Descerás na escuridão do labirinto infinito,**

**O morto,o traidor e o perdido reerguidos.**

**Ascederás ou cairás pelas mãos do rei espectral,**

**Da criança de Atena,a defesa final.**

**A destruição virá quando o último suspior do** 
**herói acontecer.**

**E perderás um amor para algo pior que morrer.** 