+++
title = 'O Mar de Monstros'
date = 2023-09-28T16:01:59-03:00
draft = false
+++
![](/images/Livro2.png)

**"Navegarás com guerreiros de osso em navio de ferro**

**O que procuras há de encontrar, e teu o tornarás**

**Mas sem esperança dirás, minha vida em pedra enterro**

**Sem amigos falharás, e voando só, retornarás."**