+++
title = 'Biografia do Autor'
date = 2021-09-18T16:59:34-03:00
draft = false
+++

![Riordan](/images/rick-riordan.jpg)

**Rick Riordan nasceu em 1964, em San Antonio,Texas, e hoje mora em Boston com a esposa e os dois filhos.Autor best-seller do The New York Times,premiado pela YALSA e pela American Library Association,por quinze anos ensinou inglês e histórias em escolas de São Francisco,e é a essa experiencia que atribui sua habilidade em escrever para o público jovem.**